package com.sample.grepexcel;

import java.io.PrintWriter;
import java.io.StringWriter;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;

/** Excel を grep するアプリケーションのメインクラス. */
@Slf4j
public class GrepExcelApplication extends Application {

  public static void main(String[] args) {
    // Desktop の機能（Excel起動）できるようにシステム設定を変更
    System.setProperty("java.awt.headless", "false");
    Application.launch(GrepExcelApplication.class, args);
  }

  private ApplicationContext context = null;

  @Override
  public void init() throws Exception {
    Thread.setDefaultUncaughtExceptionHandler(this::handleError);
    context =
        SpringApplication.run(
            GrepExcelSpringConfig.class, getParameters().getRaw().toArray(new String[0]));
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    Parent root =
        context
            .getBean(FXMLLoader.class)
            .load(getClass().getResourceAsStream("/fxml/grepExcel.fxml"));
    Scene scene = new Scene(root);

    primaryStage.setTitle("Grep Excel");
    primaryStage.setScene(scene);
    primaryStage.show();
  }

  @Override
  public void stop() throws Exception {
    SpringApplication.exit(context);
  }

  /**
   * キャッチされない例外のハンドリングを行う.
   *
   * @param t 例外の発生したスレッド
   * @param e 例外
   */
  protected void handleError(Thread t, Throwable e) {
    log.error("予期しないエラーが発生しました。", e);
    Platform.runLater(
        () -> {
          StringWriter writer = new StringWriter();
          try (PrintWriter printWriter = new PrintWriter(writer)) {
            e.printStackTrace(printWriter);
          }

          Alert alert = new Alert(Alert.AlertType.ERROR, null, ButtonType.OK);
          alert.setHeaderText("エラーが発生しました。");
          alert.setResizable(true);

          TextArea textArea = new TextArea(writer.toString());
          textArea.setEditable(false);
          alert.getDialogPane().setContent(textArea);
          alert.getDialogPane().setPrefHeight(200.0);
          alert.show();
        });
  }
}
