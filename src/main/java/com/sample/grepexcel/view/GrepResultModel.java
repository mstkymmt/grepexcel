package com.sample.grepexcel.view;

import com.sample.grepexcel.logic.MatchedText;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/** 検索結果を表示するテーブルのモデル. */
public class GrepResultModel {
  /** エクセルファイルのパス. */
  private StringProperty path = new SimpleStringProperty();
  /** エクセルファイル名. */
  private StringProperty fileName = new SimpleStringProperty();
  /** シート名. */
  private StringProperty sheetName = new SimpleStringProperty();
  /** セルのアドレス. */
  private StringProperty cellAddress = new SimpleStringProperty();
  /** 該当したセルの値の情報. */
  private ObjectProperty<MatchedText> matchedText = new SimpleObjectProperty<>();

  public final String getPath() {
    return path.get();
  }

  public final void setPath(String value) {
    path.set(value);
  }

  public StringProperty pathProperty() {
    return path;
  }

  public final String getFileName() {
    return fileName.get();
  }

  public final void setFileName(String value) {
    fileName.set(value);
  }

  public StringProperty fileNameProperty() {
    return fileName;
  }

  public final String getSheetName() {
    return sheetName.get();
  }

  public final void setSheetName(String value) {
    sheetName.set(value);
  }

  public StringProperty sheetNameProperty() {
    return sheetName;
  }

  public final String getCellAddress() {
    return cellAddress.get();
  }

  public final void setCellAddress(String value) {
    cellAddress.set(value);
  }

  public StringProperty cellAddressProperty() {
    return cellAddress;
  }

  public final MatchedText getMatchedText() {
    return matchedText.get();
  }

  public final void setMatchedText(MatchedText value) {
    matchedText.set(value);
  }

  public ObjectProperty<MatchedText> matchedTextProperty() {
    return matchedText;
  }
}
