package com.sample.grepexcel.view;

import javafx.beans.Observable;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.Property;
import javafx.concurrent.Worker;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.VBox;

/** {@link Worker} の実行と進捗の表示を行うダイアログ. */
public class ProgressBarDialog extends Dialog<ButtonType> {

  private Label messageLabel;
  private ProgressBar progressBar;
  private Button cancelButton;
  private Worker worker = null;

  /**
   * ダイアログを作成する.
   *
   * @param worker 実行する Worker
   */
  public ProgressBarDialog(Worker worker) {
    super();
    this.worker = worker;
    setResult(ButtonType.FINISH);

    getDialogPane().setContent(createContent());
    cancelButton.setOnAction(it -> this.worker.cancel());

    messageProperty().bind(worker.messageProperty());
    progressProperty().bind(worker.progressProperty());

    worker.runningProperty().addListener(this::workerRunningChanged);
  }

  /**
   * Worker の実行状態が変更されたときに呼び出される.
   *
   * @param observable 状態が変わったオブジェクト（Worker）
   */
  protected void workerRunningChanged(Observable observable) {
    if (!worker.isRunning()) {
      if (worker.getState() == Worker.State.CANCELLED) {
        setResult(ButtonType.CANCEL);
      }
      if (worker.getState() == Worker.State.FAILED && worker.getException() != null) {
        Thread.getDefaultUncaughtExceptionHandler()
            .uncaughtException(Thread.currentThread(), worker.getException());
      }
      this.close();
    }
  }

  /**
   * ダイアログのコンテントを作成する.
   *
   * @return 作成したコンテント.
   */
  private VBox createContent() {
    VBox vbox = new VBox();
    messageLabel = new Label();
    vbox.getChildren().add(messageLabel);

    progressBar = new ProgressBar();
    progressBar.setPrefWidth(400);
    vbox.getChildren().add(progressBar);

    ButtonBar buttonBar = new ButtonBar();
    cancelButton = new Button("キャンセル");
    ButtonBar.setButtonData(cancelButton, ButtonBar.ButtonData.CANCEL_CLOSE);
    buttonBar.getButtons().add(cancelButton);

    vbox.getChildren().add(buttonBar);
    VBox.setMargin(buttonBar, new Insets(10.0, 0, 0, 0));

    return vbox;
  }

  /**
   * 進捗を表すメッセージを保持するプロパティを取得する.
   *
   * @return メッセージを保持するプロパティ
   */
  public final Property<String> messageProperty() {
    return messageLabel.textProperty();
  }

  /**
   * 進捗を保持するプロパティを取得する.
   *
   * @return 進捗を保持するプロパティ
   */
  public final DoubleProperty progressProperty() {
    return progressBar.progressProperty();
  }
}
