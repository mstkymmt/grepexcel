package com.sample.grepexcel.view;

import com.sample.grepexcel.GrepExcelPreference;
import com.sample.grepexcel.exceptions.InvalidPatternException;
import com.sample.grepexcel.exceptions.OpenFileEditorException;
import com.sample.grepexcel.logic.ExcelsWalkTask;
import com.sample.grepexcel.logic.FoundCellInfo;
import com.sample.grepexcel.logic.GrepExcelTaskFactory;
import com.sample.grepexcel.logic.MatchedText;
import com.sample.grepexcel.logic.html.SearchResultHtmlFormatter;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ForkJoinPool;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.Clipboard;
import javafx.scene.input.DataFormat;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.DirectoryChooser;
import javafx.stage.Window;
import javax.swing.SwingUtilities;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/** Excel Grep画面. */
@Component
public class GrepExcelController {

  @FXML private Parent root;

  @FXML private ComboBox<String> targetDirectory;
  @FXML private ComboBox<String> patternText;
  @FXML private ComboBox<String> replacementText;
  @FXML private Label matchedCountLabel;

  @FXML private TableView<GrepResultModel> resultTable;
  @FXML private TableColumn<GrepResultModel, String> filePathColumn;
  @FXML private TableColumn<GrepResultModel, String> fileNameColumn;
  @FXML private TableColumn<GrepResultModel, String> sheetNameColumn;
  @FXML private TableColumn<GrepResultModel, String> cellAddressColumn;
  @FXML private TableColumn<GrepResultModel, MatchedText> cellValueColumn;

  private GrepExcelTaskFactory taskFactory = null;
  private SearchResultHtmlFormatter searchResultHtmlFormatter = null;
  private GrepExcelPreference preference;

  private List<FoundCellInfo> searchResults = new ArrayList<>();

  @Autowired
  public void setTaskFactory(GrepExcelTaskFactory taskFactory) {
    this.taskFactory = taskFactory;
  }

  @Autowired
  public void setSearchResultHtmlFormatter(SearchResultHtmlFormatter searchResultHtmlFormatter) {
    this.searchResultHtmlFormatter = searchResultHtmlFormatter;
  }

  @Autowired
  public void setGrepExcelPreference(GrepExcelPreference preference) {
    this.preference = preference;
  }

  @FXML
  protected void initialize() {
    filePathColumn.setCellValueFactory(new PropertyValueFactory<>("path"));
    fileNameColumn.setCellValueFactory(new PropertyValueFactory<>("fileName"));
    sheetNameColumn.setCellValueFactory(new PropertyValueFactory<>("sheetName"));
    cellAddressColumn.setCellValueFactory(new PropertyValueFactory<>("cellAddress"));
    cellValueColumn.setCellValueFactory(new PropertyValueFactory<>("matchedText"));
    cellValueColumn.setCellFactory(it -> new CellValueTableCell());

    resultTable
        .getItems()
        .addListener(
            (Observable observable) -> {
              matchedCountLabel.setText(String.format("(該当 %,d 件)", resultTable.getItems().size()));
            });
    targetDirectory.getItems().addAll(preference.getTargetDirectoryHistory());
    patternText.getItems().addAll(preference.getPatternHistory());
    replacementText.getItems().addAll(preference.getReplacementHistory());
  }

  /** 対象ディレクトリの参照ボタンをクリックしたときの処理. */
  @FXML
  protected void onBrowseDirectoryClicked() {
    DirectoryChooser chooser = new DirectoryChooser();
    chooser.setTitle("処理対象のディレクトリ選択");

    if (targetDirectory.getValue() != null) {
      File oldFile = new File(targetDirectory.getValue());
      if (oldFile.isDirectory()) {
        chooser.setInitialDirectory(oldFile);
      }
    }

    File file = chooser.showDialog(getWindow());
    if (file != null) {
      targetDirectory.setValue(file.getAbsolutePath());
    }
  }

  /** 一括置換. */
  @FXML
  protected void onExecuteReplaceAll() {
    if (!validateInput()) {
      return;
    }

    ExcelsWalkTask<List<FoundCellInfo>> task;
    try {
      task =
          taskFactory.createReplaceTask(
              new File(targetDirectory.getValue()),
              StringUtils.defaultString(patternText.getValue()),
              StringUtils.defaultString(replacementText.getValue()));
    } catch (InvalidPatternException e) {
      showPatternError(e);
      return;
    }

    Alert confirm =
        new Alert(Alert.AlertType.INFORMATION, "一括置換を行います。", ButtonType.OK, ButtonType.CANCEL);
    confirm.setHeaderText(null);
    if (confirm.showAndWait().orElse(null) != ButtonType.OK) {
      return;
    }

    updateInputHistories();

    task.setOnSucceeded(
        event -> {
          setSearchResults(task.getValue());

          Alert alert = new Alert(Alert.AlertType.INFORMATION, "置換が完了しました。", ButtonType.OK);
          alert.setHeaderText(null);
          alert.showAndWait();
        });

    ProgressBarDialog dialog = new ProgressBarDialog(task);
    ForkJoinPool.commonPool().execute(task);
    dialog.showAndWait();
  }

  /** 検索. */
  @FXML
  protected void onSearch() {
    if (!validateInput()) {
      return;
    }

    ExcelsWalkTask<List<FoundCellInfo>> task;
    try {
      task =
          taskFactory.createSearchTask(
              new File(targetDirectory.getValue()),
              StringUtils.defaultString(patternText.getValue()));
    } catch (InvalidPatternException e) {
      showPatternError(e);
      return;
    }
    updateInputHistories();

    task.setOnSucceeded(event -> setSearchResults(task.getValue()));

    ProgressBarDialog dialog = new ProgressBarDialog(task);
    ForkJoinPool.commonPool().execute(task);
    dialog.showAndWait();
  }

  /**
   * 検索結果一覧の情報を作成する.
   *
   * @param info 検索したセルの情報
   * @return 検索結果テーブルのモデル
   */
  protected GrepResultModel createGrepResultModel(FoundCellInfo info) {
    GrepResultModel model = new GrepResultModel();
    model.setPath(info.getFile().getAbsolutePath());
    model.setFileName(info.getFile().getName());
    model.setSheetName(info.getSheetName());
    model.setCellAddress(info.getCellAddress().formatAsString());
    model.setMatchedText(info.getMatchedText());
    return model;
  }

  /**
   * 検索結果を設定する.
   *
   * @param results 検索結果
   */
  protected void setSearchResults(List<FoundCellInfo> results) {
    resultTable.getItems().clear();
    for (FoundCellInfo foundInfo : results) {
      resultTable.getItems().add(createGrepResultModel(foundInfo));
    }
    searchResults = results;
  }

  @FXML
  protected void copyResults() {
    String html = searchResultHtmlFormatter.format(searchResults);
    Map<DataFormat, Object> content = new HashMap<>();
    content.put(DataFormat.PLAIN_TEXT, html);
    content.put(DataFormat.HTML, html);
    Clipboard.getSystemClipboard().setContent(content);
  }

  @FXML
  @SuppressWarnings("unchecked")
  protected void onClickResultTable(MouseEvent event) throws IOException {
    if (event.getClickCount() != 2 || event.getButton() != MouseButton.PRIMARY) {
      return;
    }

    // クリックしたデータ行を取得
    TableRow<GrepResultModel> row = null;
    Node node = ((Node) event.getTarget());
    while (node != null) {
      if (node instanceof TableRow) {
        row = (TableRow<GrepResultModel>) node;
        break;
      }
      node = node.getParent();
    }

    if (row != null && row.getItem() != null) {
      File targetFile = new File(row.getItem().getPath());
      SwingUtilities.invokeLater(
          () -> {
            try {
              Desktop.getDesktop().open(targetFile);
            } catch (IOException ex) {
              throw new OpenFileEditorException(
                  "システムのエディタでファイルのオープンに失敗しました。 対象ファイル：" + targetFile, ex);
            }
          });
    }
  }

  /**
   * ウィンドウを取得する.
   *
   * @return ウィンドウ
   */
  protected Window getWindow() {
    return root.getScene().getWindow();
  }

  /** 入力履歴を更新する. */
  protected void updateInputHistories() {
    preference.setTargetDirectoryHistory(updateInputHistory(targetDirectory));
    preference.setPatternHistory(updateInputHistory(patternText));
    preference.setReplacementHistory(updateInputHistory(replacementText));
  }

  /**
   * 指定したコンボボックスの入力を履歴（リスト）に反映する.
   *
   * @param target コンボボックス
   * @return 新しい履歴
   */
  private List<String> updateInputHistory(ComboBox<String> target) {
    String value = target.getValue();
    List<String> histories = getNewHistoryList(value, target.getItems());
    target.getItems().clear();
    target.getItems().addAll(histories);
    target.setValue(value);
    return histories;
  }

  /**
   * 新しい入力履歴のリストを取得する.
   *
   * @param newValue 新しく入力した値
   * @param oldHistories 既存の履歴
   * @return 新しい履歴
   */
  private List<String> getNewHistoryList(String newValue, List<String> oldHistories) {
    Set<String> histories = new LinkedHashSet<>();
    histories.add(newValue);
    histories.addAll(oldHistories);

    List<String> historyList = new ArrayList<>();
    for (String history : histories) {
      historyList.add(history);
      if (historyList.size() > 10) {
        break;
      }
    }
    return historyList;
  }

  /**
   * 検索パターンの入力が不正な場合のエラーを画面に表示する.
   *
   * @param e エラー
   */
  private void showPatternError(InvalidPatternException e) {
    Alert alert = new Alert(Alert.AlertType.ERROR);
    alert.setHeaderText("検索 の入力が不正です。");
    alert.setContentText(e.getMessage());
    alert.show();
  }

  private boolean validateInput() {
    List<String> errors = new ArrayList<>();
    if (StringUtils.isBlank(targetDirectory.getValue())) {
      errors.add("ディレクトリを入力してください");
    }
    if (StringUtils.isBlank(patternText.getValue())) {
      errors.add("検索を入力してください");
    }

    if (!errors.isEmpty()) {
      Alert alert = new Alert(Alert.AlertType.ERROR);
      alert.setHeaderText("入力エラー");
      ListView<String> listView = new ListView<>(FXCollections.observableList(errors));
      alert.getDialogPane().setPrefHeight(200.0);
      alert.getDialogPane().setContent(listView);
      alert.show();
    }

    return errors.isEmpty();
  }
}
