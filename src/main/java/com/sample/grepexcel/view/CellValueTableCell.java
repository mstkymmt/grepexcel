package com.sample.grepexcel.view;

import com.sample.grepexcel.logic.MatchedText;
import com.sample.grepexcel.logic.Range;
import javafx.scene.control.TableCell;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

/** 該当したエクセルのセルの値を表示するテーブルのセル. */
public class CellValueTableCell extends TableCell<GrepResultModel, MatchedText> {

  public CellValueTableCell() {
    widthProperty().addListener(event -> recalcCellWidth());
  }

  @Override
  protected void updateItem(MatchedText item, boolean empty) {
    super.updateItem(item, empty);
    if (empty || item == null) {
      setGraphic(null);
      return;
    }

    TextFlow textFlow = new TextFlow();
    int current = 0;
    for (Range range : item.getMatchedRanges()) {
      if (current < range.getStart()) {
        Text noMatchedTextNode = new Text(item.getText().substring(current, range.getStart()));
        noMatchedTextNode.setFill(Color.BLACK);
        textFlow.getChildren().add(noMatchedTextNode);
      }

      Text matchedTextNode = new Text(item.getText().substring(range.getStart(), range.getEnd()));
      matchedTextNode.setFill(Color.RED);
      textFlow.getChildren().add(matchedTextNode);
      current = range.getEnd();
    }
    if (current < item.getText().length()) {
      Text tailTextNode = new Text(item.getText().substring(current));
      tailTextNode.setFill(Color.BLACK);
      textFlow.getChildren().add(tailTextNode);
    }
    setGraphic(textFlow);
    recalcCellWidth();
  }

  /** セルの幅を計算する. */
  protected void recalcCellWidth() {
    if (getGraphic() != null) {
      // 当該セルの width は初回は 0.0 になっている可能性があるので、TableColumn から取得する
      double clientWidth =
          getTableColumn().getWidth() - (getInsets().getLeft() + getInsets().getRight());
      setPrefHeight(
          getGraphic().prefHeight(clientWidth) + getInsets().getTop() + getInsets().getBottom());
    } else {
      setPrefHeight(USE_COMPUTED_SIZE);
    }
  }
}
