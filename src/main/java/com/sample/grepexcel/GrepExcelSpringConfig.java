package com.sample.grepexcel;

import freemarker.template.Configuration;
import javafx.fxml.FXMLLoader;
import javax.annotation.PostConstruct;
import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class GrepExcelSpringConfig {

  @Bean
  public FXMLLoader fxmlLoader(BeanFactory beanFactory) {
    FXMLLoader loader = new FXMLLoader();
    loader.setControllerFactory(beanFactory::getBean);
    return loader;
  }

  @Bean
  public Configuration freemarkerConfiguration() {
    Configuration config = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
    config.setClassForTemplateLoading(getClass(), "/freemarker");
    config.setDefaultEncoding("UTF-8");
    return config;
  }

  @PostConstruct
  public void setupMinInflateRatio() {
    ZipSecureFile.setMinInflateRatio(0.0075d);
  }
}
