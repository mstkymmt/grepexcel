package com.sample.grepexcel;

import java.util.Collections;
import java.util.List;
import java.util.prefs.Preferences;
import org.springframework.stereotype.Component;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

/** Excel を grep するアプリケーションのユーザ設定を保持する. */
@Component
public class GrepExcelPreference {
  private Preferences preference = Preferences.userNodeForPackage(GrepExcelPreference.class);

  /**
   * 保存用の値の作成と解析を行う Yaml オブジェクトを作成する.
   *
   * @return 作成したオブジェクト
   */
  protected Yaml createYaml() {
    DumperOptions options = new DumperOptions();
    options.setDefaultFlowStyle(DumperOptions.FlowStyle.FLOW);
    return new Yaml(options);
  }

  /**
   * 対象ディレクトリの入力履歴を取得する.
   *
   * @param targetDirectories 対象ディレクトリの入力履歴
   */
  public void setTargetDirectoryHistory(List<String> targetDirectories) {
    preference.put("history.targetDirectory", createYaml().dump(targetDirectories));
  }

  /**
   * 対象ディレクトリの入力履歴を保存する.
   *
   * @return 対象ディレクトリの入力履歴
   */
  public List<String> getTargetDirectoryHistory() {
    String value = preference.get("history.targetDirectory", null);
    if (value == null) {
      return Collections.emptyList();
    }
    return createYaml().load(value);
  }

  /**
   * 検索パターンの入力履歴を取得する.
   *
   * @return 検索パターンの入力履歴
   */
  public List<String> getPatternHistory() {
    String value = preference.get("history.pattern", null);
    if (value == null) {
      return Collections.emptyList();
    }
    return createYaml().load(value);
  }

  /**
   * 検索パターンの入力履歴を保存する.
   *
   * @param patternHistory 検索パターンの入力履歴
   */
  public void setPatternHistory(List<String> patternHistory) {
    preference.put("history.pattern", createYaml().dump(patternHistory));
  }

  /**
   * 置換パターンの入力履歴を取得する.
   *
   * @return 置換パターンの入力履歴
   */
  public List<String> getReplacementHistory() {
    String value = preference.get("history.replacement", null);
    if (value == null) {
      return Collections.emptyList();
    }
    return createYaml().load(value);
  }

  /**
   * 置換パターンの入力履歴を保存する.
   *
   * @param replacementHistory 置換パターンの入力履歴
   */
  public void setReplacementHistory(List<String> replacementHistory) {
    preference.put("history.replacement", createYaml().dump(replacementHistory));
  }
}
