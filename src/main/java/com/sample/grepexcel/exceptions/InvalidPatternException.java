package com.sample.grepexcel.exceptions;

public class InvalidPatternException extends IllegalArgumentException {
  public InvalidPatternException(String message) {
    super(message);
  }

  public InvalidPatternException(String message, Throwable cause) {
    super(message, cause);
  }
}
