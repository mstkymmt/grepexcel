package com.sample.grepexcel.exceptions;

/** 指定したファイルに対応するアプリケーションを起動できない場合の例外. */
public class OpenFileEditorException extends IllegalArgumentException {
  public OpenFileEditorException(String message) {
    super(message);
  }

  public OpenFileEditorException(String message, Throwable cause) {
    super(message, cause);
  }
}
