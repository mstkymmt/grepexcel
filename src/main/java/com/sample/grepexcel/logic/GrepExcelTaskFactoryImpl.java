package com.sample.grepexcel.logic;

import com.sample.grepexcel.exceptions.InvalidPatternException;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import org.springframework.stereotype.Component;

/**
 * Excel の一括検索・置換処理を行うタスクを作成するファクトリの実装クラス.
 *
 * <p>正規表現による検索・置換処理を実行するタスクを作成する.
 */
@Component
public class GrepExcelTaskFactoryImpl implements GrepExcelTaskFactory {

  @Override
  public ExcelsWalkTask<List<FoundCellInfo>> createSearchTask(
      File targetDirectory, String pattern) {
    GrepProcessor processor = new GrepProcessor();
    processor.setPattern(compilePattern(pattern));
    ExcelsWalkTask<List<FoundCellInfo>> task = new ExcelsWalkTask<>(targetDirectory, processor);
    task.setReadOnly(true);
    return task;
  }

  @Override
  public ExcelsWalkTask<List<FoundCellInfo>> createReplaceTask(
      File targetDirectory, String pattern, String replacement) {
    GrepProcessor processor = new GrepProcessor();
    processor.setPattern(compilePattern(pattern));
    processor.setReplacement(filterReplacement(replacement));
    return new ExcelsWalkTask<>(targetDirectory, processor);
  }

  /**
   * 検索パターンをコンパイルする.
   *
   * @param pattern 検索パターン
   * @return コンパイルしたパターン
   */
  protected Pattern compilePattern(String pattern) {
    try {
      return Pattern.compile(pattern);
    } catch (PatternSyntaxException e) {
      throw new InvalidPatternException(e.getMessage(), e);
    }
  }

  /** 置換文字列でエスケープの後に指定することで特殊な意味を持つ文字. */
  private static final Map<String, String> SPECIAL_CHAR_MAP = new HashMap<>();

  static {
    SPECIAL_CHAR_MAP.put("n", "\n");
    SPECIAL_CHAR_MAP.put("r", "\r");
    SPECIAL_CHAR_MAP.put("t", "\t");
  }

  /**
   * 検索パターンをコンパイルする.
   *
   * @param replacement 検索パターン
   * @return コンパイルしたパターン
   */
  protected String filterReplacement(String replacement) {
    Matcher matcher = Pattern.compile("\\\\(.)").matcher(replacement);
    StringBuffer result = new StringBuffer();
    while (matcher.find()) {
      String specialChar = SPECIAL_CHAR_MAP.get(matcher.group(1));
      if (specialChar != null) {
        matcher.appendReplacement(result, specialChar);
      } else {
        matcher.appendReplacement(result, "$0");
      }
    }
    matcher.appendTail(result);
    return result.toString();
  }
}
