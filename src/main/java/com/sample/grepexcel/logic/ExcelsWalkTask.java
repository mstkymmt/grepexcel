package com.sample.grepexcel.logic;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javafx.concurrent.Task;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.util.IOUtils;

/** 指定したディレクトリ以下にあるエクセルファイルを一括処理するタスク. */
@Slf4j
public class ExcelsWalkTask<T> extends Task<T> {
  private static final Pattern EXCEL_FILENAME_PATTERN = Pattern.compile("(?i).*\\.xls[a-z]?");
  private final File root;
  private final ExcelProcessor<T> processor;
  private List<File> targetFiles = Collections.emptyList();
  private boolean readOnly = false;

  /**
   * <code>root</code> 以下にあるエクセルファイルを一括処理するオブジェクトを作成する.
   *
   * @param root 処理対象のルートディレクトリ
   * @param processor 各エクセルファイルに対して行う処理
   */
  public ExcelsWalkTask(File root, ExcelProcessor<T> processor) {
    this.root = root;
    this.processor = processor;
  }

  /**
   * エクセルを読み込み専用で扱うかを取得する.
   *
   * @return 読み込み専用の場合は true、更新可の場合は false.
   */
  public boolean isReadOnly() {
    return readOnly;
  }

  /**
   * エクセルを読み込み専用で扱うかを指定する.
   *
   * @param readOnly 読み込み専用の場合は true、更新可の場合は false.
   */
  public void setReadOnly(boolean readOnly) {
    this.readOnly = readOnly;
  }

  /**
   * 処理対象のエクセルファイルを取得する.
   *
   * @return エクセルファイルのリスト
   */
  protected List<File> getExcelFiles() {
    try {
      return Files.walk(root.toPath())
          .map(Path::toFile)
          .filter(this::isExcelFile)
          .collect(Collectors.toList());
    } catch (IOException e) {
      throw new IllegalArgumentException("ルートディレクトリ " + root + " が不正です", e);
    }
  }

  /**
   * ファイルが処理対象（エクセルファイル）かを取得する.
   *
   * @param file ファイル
   * @return 処理対象の場合は true、それ以外は false.
   */
  protected boolean isExcelFile(File file) {
    return (EXCEL_FILENAME_PATTERN.matcher(file.getName()).matches())
        && !file.getName().startsWith("~");
  }

  /**
   * エクセルファイルに対して処理を実行する.
   *
   * @param file 処理対象のファイル
   */
  protected void processWorkBook(File file) {
    log.info(root.toPath().relativize(file.toPath()) + "：処理開始");

    Workbook workbook;
    try (InputStream inStream = Files.newInputStream(file.toPath())) {
      try {
        workbook = WorkbookFactory.create(inStream, null);
      } catch (EncryptedDocumentException | IOException e) {
        throw new RuntimeException("Excel ファイルのオープンに失敗しました", e);
      }
    } catch (IOException e) {
      throw new RuntimeException("Excel ファイルのオープンに失敗しました", e);
    }

    File updatedFile = null;
    try {
      if (processor.process(file, workbook) && !isReadOnly()) {
        updatedFile = writeWorkbook(file, workbook);
      }
    } finally {
      IOUtils.closeQuietly(workbook);
    }

    if (updatedFile != null) {
      file.delete();
      updatedFile.renameTo(file);
    }

    log.info(root.toPath().relativize(file.toPath()) + "：終了");
  }

  /**
   * <code>workbook</code> を <code>file</code> と同じディレクトリの一時ファイルに出力する.
   *
   * @param file ファイル
   * @param workbook ワークブック
   * @return 出力した一時ファイル
   */
  private File writeWorkbook(File file, Workbook workbook) {
    try {
      File tempFile = File.createTempFile("newfile", ".tmp", file.getParentFile());
      try (OutputStream output = new BufferedOutputStream(new FileOutputStream(tempFile))) {
        workbook.write(output);
      }
      return tempFile;
    } catch (IOException e) {
      throw new RuntimeException("更新後のファイル出力に失敗しました.", e);
    }
  }

  @Override
  protected T call() throws Exception {
    targetFiles = getExcelFiles();
    int progress = 0;
    for (File file : targetFiles) {
      updateRunningState(progress, file);
      processWorkBook(file);
      progress++;
    }
    return processor.getResult();
  }

  /**
   * タスクのステータスを更新する.
   *
   * @param progress 処理済のファイル数.
   * @param file 現在処理中のファイル
   */
  protected void updateRunningState(int progress, File file) {
    updateMessage(String.format("処理中 (%d / %d)：%s", progress, targetFiles.size(), file.getName()));
    updateProgress(progress, targetFiles.size());
  }
}
