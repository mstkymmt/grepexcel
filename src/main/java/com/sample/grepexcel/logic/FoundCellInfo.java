package com.sample.grepexcel.logic;

import java.io.File;
import lombok.Data;
import org.apache.poi.ss.util.CellAddress;

/** セルの検索結果を保持する. */
@Data
public class FoundCellInfo {
  /** エクセルファイル. */
  private File file;
  /** シート名. */
  private String sheetName;
  /** セルのアドレス. */
  private CellAddress cellAddress;
  /** 該当した文字列の情報. */
  private MatchedText matchedText;
}
