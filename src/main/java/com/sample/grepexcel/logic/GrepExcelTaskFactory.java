package com.sample.grepexcel.logic;

import com.sample.grepexcel.exceptions.InvalidPatternException;
import java.io.File;
import java.util.List;

/** Excel の一括検索・置換処理を行うタスクを作成するファクトリ. */
public interface GrepExcelTaskFactory {

  /**
   * 一括検索（Grep）を行うタスクを作成する.
   *
   * @param targetDirectory 対象ディレクトリ
   * @param pattern 検索パターン
   * @return 作成したタスク
   * @throws InvalidPatternException 検索パターンが不正な場合
   */
  ExcelsWalkTask<List<FoundCellInfo>> createSearchTask(File targetDirectory, String pattern)
      throws InvalidPatternException;

  /**
   * 一括置換を行うタスクを作成する.
   *
   * @param targetDirectory 対象ディレクトリ
   * @param pattern 検索パターン
   * @param replacement 置換する文字列
   * @return 作成したタスク
   * @throws InvalidPatternException 検索パターンが不正な場合
   */
  ExcelsWalkTask<List<FoundCellInfo>> createReplaceTask(
      File targetDirectory, String pattern, String replacement) throws InvalidPatternException;
}
