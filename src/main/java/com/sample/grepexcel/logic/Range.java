package com.sample.grepexcel.logic;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/** 範囲を保持する. */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Range {
  /** 開始位置. */
  private int start;
  /** 終了位置. */
  private int end;
}
