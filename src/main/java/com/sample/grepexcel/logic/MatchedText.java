package com.sample.grepexcel.logic;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;

/** 該当した値（文字列）の情報を保持する. */
@Data
public class MatchedText {
  /** 該当した部分を含む全体の文字列. */
  private String text;
  /** 該当した範囲のリスト. */
  private List<Range> matchedRanges = new ArrayList<>();
}
