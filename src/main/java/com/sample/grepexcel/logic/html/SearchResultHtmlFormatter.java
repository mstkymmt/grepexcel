package com.sample.grepexcel.logic.html;

import com.sample.grepexcel.logic.FoundCellInfo;
import freemarker.template.Configuration;
import freemarker.template.Template;
import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/** 結果の一覧を HTML に変換する. */
@Component
public class SearchResultHtmlFormatter {

  private String templateName = "resultHtml.ftl";
  private ResultTemplateUtil resultTemplateUtil = new ResultTemplateUtil();
  private Configuration freemarkerConfiguration;

  /**
   * HTML変換に使用するテンプレートの名前を設定する.
   *
   * @param templateName テンプレート名
   */
  public void setTemplateName(String templateName) {
    this.templateName = templateName;
  }

  /**
   * HTML変換時にテンプレートで使用するユーティリティを設定する.
   *
   * @param resultTemplateUtil ユーティリティ
   */
  public void setResultTemplateUtil(ResultTemplateUtil resultTemplateUtil) {
    this.resultTemplateUtil = resultTemplateUtil;
  }

  /**
   * FreeMarker の設定情報を設定する.
   *
   * @param freemarkerConfiguration FreeMarker の設定情報
   */
  @Autowired
  public void setFreemarkerConfiguration(Configuration freemarkerConfiguration) {
    this.freemarkerConfiguration = freemarkerConfiguration;
  }

  /**
   * 結果を HTML に変換する.
   *
   * @param results 検索結果のリスト
   * @return HTML文字列
   */
  public String format(List<FoundCellInfo> results) {
    try {
      Template template = freemarkerConfiguration.getTemplate(templateName);

      StringWriter writer = new StringWriter();
      template.process(createContext(results), writer);
      return writer.toString();
    } catch (freemarker.template.TemplateException | IOException e) {
      throw new TemplateException("テンプレートの処理に失敗しました.", e);
    }
  }

  /**
   * テンプレートに渡す情報を作成する.
   *
   * @param results 検索結果のリスト
   * @return テンプレートに渡す情報
   */
  protected Map<String, Object> createContext(List<FoundCellInfo> results) {
    Map<String, Object> context = new HashMap<>();
    context.put("results", results);
    context.put("util", resultTemplateUtil);
    return context;
  }
}
