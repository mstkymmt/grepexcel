package com.sample.grepexcel.logic.html;

/** テンプレートに問題がある場合の例外. */
public class TemplateException extends RuntimeException {

  /**
   * メッセージを指定して例外を作成する.
   *
   * @param message メッセージ
   */
  public TemplateException(String message) {
    super(message);
  }

  /**
   * メッセージと原因を指定して例外を作成する.
   *
   * @param message メッセージ
   * @param cause 原因
   */
  public TemplateException(String message, Throwable cause) {
    super(message, cause);
  }
}
