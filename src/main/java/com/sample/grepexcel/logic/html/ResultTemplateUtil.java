package com.sample.grepexcel.logic.html;

import com.sample.grepexcel.logic.FoundCellInfo;
import com.sample.grepexcel.logic.MatchedText;
import com.sample.grepexcel.logic.Range;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

/** 結果のHTML出力で使用するユーティリティ群. */
public class ResultTemplateUtil {

  /**
   * 指定したセルへリンク URL を取得する.
   *
   * @param cellInfo セル情報
   * @return リンク URL
   */
  public String getLinkUrl(FoundCellInfo cellInfo) {
    StringBuilder builder = new StringBuilder();
    builder.append(cellInfo.getFile().toURI().toString());
    builder.append("#").append(encodeSheetName(cellInfo.getSheetName()));
    builder.append("!").append(cellInfo.getCellAddress().toString());
    return builder.toString();
  }

  /**
   * シート名をリンクで使用する形式にエンコードする.
   *
   * @param sheetName シート名
   * @return エンコードしたシート名
   */
  private String encodeSheetName(String sheetName) {
    return "'" + sheetName.replace("'", "''") + "'";
  }

  /**
   * テキストのマッチ情報を出力用にフォーマットする.
   *
   * @param matchedText マッチ情報
   * @return 出力用 HTML 文字列.
   */
  public String formatMatchedText(MatchedText matchedText) {
    StringBuilder builder = new StringBuilder();
    int current = 0;
    for (Range range : matchedText.getMatchedRanges()) {
      builder.append(
          StringEscapeUtils.escapeHtml4(
              StringUtils.substring(matchedText.getText(), current, range.getStart())));
      builder.append("<span class='matched'>");
      builder.append(
          StringEscapeUtils.escapeHtml4(
              StringUtils.substring(matchedText.getText(), range.getStart(), range.getEnd())));
      builder.append("</span>");
      current = range.getEnd();
    }
    builder.append(
        StringEscapeUtils.escapeHtml4(StringUtils.substring(matchedText.getText(), current)));
    return builder.toString();
  }
}
