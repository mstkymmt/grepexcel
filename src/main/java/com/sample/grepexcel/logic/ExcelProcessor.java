package com.sample.grepexcel.logic;

import java.io.File;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * エクセルファイルを処理するインターフェイス.
 *
 * @param <T> 結果のタイプ
 */
public interface ExcelProcessor<T> {

  /**
   * エクセルのワークブックの処理を行う.
   *
   * @param file エクセルファイル
   * @param workbook ワークブック
   * @return wookbook を変更した場合は true、それ以外は false.
   */
  boolean process(File file, Workbook workbook);

  /**
   * 結果を取得する.
   *
   * @return 結果
   */
  T getResult();
}
