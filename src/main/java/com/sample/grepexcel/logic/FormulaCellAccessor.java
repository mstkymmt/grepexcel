package com.sample.grepexcel.logic;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.BuiltinFormats;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;

/**
 * 数式を対象にセルの値を取得・設定する.
 *
 * <p>エクセルの検索・置換で検索対象「数式」を選択したときのような動作となる.
 */
public class FormulaCellAccessor implements CellAccessor {

  // 時刻部分で使用する書式のパターン.
  private static final Pattern TIME_PART_PATTERN = Pattern.compile("(?i)(?:h.*m|m.*s)");

  // 日時（もしくは日付のみ）を指定する文字列のパターン.
  private static final Pattern DATETIME_VALUE_PATTERN =
      Pattern.compile("^(\\d{1,4}/\\d{1,2}/\\d{1,2})(?: +(\\d{1,2}:\\d{1,2}(?::\\d{1,2})?))?$");

  // 時刻を持ったセルの値を文字列に変換する書式.
  private DateFormat timeFormat = new SimpleDateFormat("H:mm:ss");
  // 日付を持ったセルの値を文字列に変換する書式.
  private DateFormat dateFormat = new SimpleDateFormat("yyyy/M/d");
  // 時刻を持ったセルの値を文字列に変換する書式.
  private DateFormat dateTimeFormat = new SimpleDateFormat("yyyy/M/d H:mm:ss");

  /** セルに値を設定する処理. */
  private interface CellValueSetter {

    /**
     * 指定したセルに値を設定する.
     *
     * @param cell 設定対象のセル
     * @param value 設定する値
     * @return 値を設定できた場合は true、それ以外は false.
     */
    boolean setValue(Cell cell, String value);
  }

  // セルに値を設定するメソッド群.
  private List<CellValueSetter> cellValueSetters = new ArrayList<>();

  public FormulaCellAccessor() {
    cellValueSetters.add(this::setNumericValue);
    cellValueSetters.add(this::setExcelDateValue);
    cellValueSetters.add(this::setFormulaValue);
    cellValueSetters.add(this::setBooleanValue);
    cellValueSetters.add(this::setStringValue);
  }

  @Override
  public String getCellString(Cell cell) {
    String value = null;
    switch (cell.getCellType()) {
      case STRING:
        value = cell.getStringCellValue();
        if (cell.getCellStyle().getQuotePrefixed()) {
          value = "'" + value;
        }
        break;

      case FORMULA:
        value = "=" + cell.getCellFormula();
        break;

      case BOOLEAN:
        value = Boolean.toString(cell.getBooleanCellValue()).toUpperCase(Locale.ENGLISH);
        break;

      case NUMERIC:
        if (DateUtil.isCellDateFormatted(cell)) {
          value = getDateCellFormat(cell).format(DateUtil.getJavaDate(cell.getNumericCellValue()));
        } else {
          value = Double.toString(cell.getNumericCellValue());
        }
        break;

      default:
        break;
    }

    return value;
  }

  @Override
  public void setCellString(Cell cell, String value) {
    if (StringUtils.isEmpty(value)) {
      cell.setCellValue((String) null);
    }

    for (CellValueSetter cellValueSetter : cellValueSetters) {
      if (cellValueSetter.setValue(cell, value)) {
        break;
      }
    }
  }

  /**
   * String のセルの値を設定する.
   *
   * @param cell セル
   * @param value 設定文字列
   * @return 設定した場合は true, それ以外は false.
   */
  protected boolean setStringValue(Cell cell, String value) {
    if (StringUtils.startsWith(value, "'")) {
      cell.getCellStyle().setQuotePrefixed(true);
      value = value.substring(1);
    } else {
      cell.getCellStyle().setQuotePrefixed(false);
    }
    cell.setCellValue(value);
    return true;
  }

  /**
   * String のセルの値を設定する.
   *
   * @param cell セル
   * @param value 設定文字列
   * @return 設定した場合は true, それ以外は false.
   */
  protected boolean setBooleanValue(Cell cell, String value) {
    if (!"TRUE".equalsIgnoreCase(value) && !"FALSE".equalsIgnoreCase(value)) {
      return false;
    }
    cell.setCellValue("TRUE".equalsIgnoreCase(value));
    return true;
  }

  /**
   * 数式のセルの値を設定する.
   *
   * @param cell セル
   * @param value 設定文字列
   * @return 設定した場合は true, それ以外は false.
   */
  protected boolean setFormulaValue(Cell cell, String value) {
    if (!StringUtils.startsWith(value, "=")) {
      return false;
    }
    cell.setCellFormula(value.substring(1));
    return true;
  }

  /**
   * 数値型のセルの値を設定する.
   *
   * @param cell セル
   * @param value 設定文字列
   * @return 設定した場合は true, それ以外は false.
   */
  protected boolean setNumericValue(Cell cell, String value) {
    try {
      cell.setCellValue(Double.parseDouble(value));
    } catch (NumberFormatException e) {
      return false;
    }

    if (DateUtil.isCellDateFormatted(cell)) {
      cell.getCellStyle().setDataFormat((short) BuiltinFormats.getBuiltinFormat("General"));
    }
    return true;
  }

  /**
   * 日時を持つセルに値を設定する.
   *
   * @param cell セル
   * @param value 値
   * @return 設定した場合は true, それ以外は false.
   */
  protected boolean setExcelDateValue(Cell cell, String value) {
    String timePart;
    int dateStyle;
    double excelDate = 0.0d;
    Matcher matcher = DATETIME_VALUE_PATTERN.matcher(value);
    if (matcher.find()) {
      try {
        excelDate = DateUtil.getExcelDate(dateFormat.parse(matcher.group(1)));
      } catch (ParseException e) {
        return false;
      }

      timePart = matcher.group(2);
      if (StringUtils.isNotEmpty(timePart)) {
        dateStyle = BuiltinFormats.getBuiltinFormat("m/d/yy h:mm");
      } else {
        dateStyle = BuiltinFormats.getBuiltinFormat("m/d/yy");
      }
    } else {
      timePart = value;
      dateStyle = BuiltinFormats.getBuiltinFormat("h:mm:ss");
    }

    if (StringUtils.isNotEmpty(timePart)) {
      try {
        excelDate += DateUtil.convertTime(timePart);
      } catch (IllegalArgumentException e) {
        return false;
      }
    }

    cell.setCellValue(excelDate);
    if (!DateUtil.isCellDateFormatted(cell)) {
      cell.getCellStyle().setDataFormat((short) dateStyle);
    }
    return true;
  }

  /**
   * 日時を持ったセルを文字列に変換するための書式を取得する.
   *
   * @param cell セル
   * @return 文字列に変換するための書式
   */
  protected DateFormat getDateCellFormat(Cell cell) {
    DateFormat format;
    boolean existsTimePart =
        TIME_PART_PATTERN.matcher(cell.getCellStyle().getDataFormatString()).find();
    boolean existsDatePart = cell.getNumericCellValue() >= 1.0d;
    if (existsDatePart) {
      if (existsTimePart) {
        format = dateTimeFormat;
      } else {
        format = dateFormat;
      }
    } else {
      format = timeFormat;
    }
    return format;
  }
}
