package com.sample.grepexcel.logic;

import org.apache.poi.ss.usermodel.Cell;

/** セルの値を文字列で設定・取得を行うインターフェイス. */
public interface CellAccessor {

  /**
   * 指定したセルの値を文字列で取得する.
   *
   * @param cell セル
   * @return 取得した値. 取得できない場合は null.
   */
  String getCellString(Cell cell);

  /**
   * セルに値を設定する.
   *
   * @param cell セル
   * @param value 設定する値
   */
  void setCellString(Cell cell, String value);
}
