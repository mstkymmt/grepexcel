package com.sample.grepexcel.logic;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Component;

/** Excel ファイル内の検索・置換を行う. */
@Slf4j
@Component
public class GrepProcessor implements ExcelProcessor<List<FoundCellInfo>> {

  private List<FoundCellInfo> foundInfos = new ArrayList<>();
  private Pattern pattern = null;
  private String replacement = null;
  private CellAccessor cellAccessor = new FormulaCellAccessor();

  @Override
  public boolean process(File file, Workbook workbook) {
    boolean modified = false;
    if (pattern == null) {
      return modified;
    }

    for (Sheet sheet : workbook) {
      for (Row row : sheet) {
        for (Cell cell : row) {
          if (processCell(file, cell)) {
            modified = true;
          }
        }
      }
    }
    return modified;
  }

  /**
   * 指定したセルのついて検索・置換を行う.
   *
   * @param file エクセルファイル
   * @param cell 対象セル
   * @return 置換した場合は true、それ以外は false.
   */
  protected boolean processCell(File file, Cell cell) {
    boolean modified = false;
    String value = cellAccessor.getCellString(cell);
    if (value == null) {
      return modified;
    }

    Matcher matcher = pattern.matcher(value);
    MatchedText matchedText = new MatchedText();
    matchedText.setText(value);
    while (matcher.find()) {
      matchedText.getMatchedRanges().add(new Range(matcher.start(), matcher.end()));
    }

    if (matchedText.getMatchedRanges().isEmpty()) {
      return modified;
    }

    FoundCellInfo info = new FoundCellInfo();
    info.setFile(file);
    info.setSheetName(cell.getSheet().getSheetName());
    info.setCellAddress(cell.getAddress());
    info.setMatchedText(matchedText);
    foundInfos.add(info);

    if (replacement != null) {
      String newValue = matcher.replaceAll(replacement);
      if (!value.equals(newValue)) {
        cellAccessor.setCellString(cell, newValue);
        modified = true;
      }
    }
    return modified;
  }

  public Pattern getPattern() {
    return pattern;
  }

  public void setPattern(Pattern pattern) {
    this.pattern = pattern;
  }

  public String getReplacement() {
    return replacement;
  }

  public void setReplacement(String replacement) {
    this.replacement = replacement;
  }

  @Override
  public List<FoundCellInfo> getResult() {
    return foundInfos;
  }
}
