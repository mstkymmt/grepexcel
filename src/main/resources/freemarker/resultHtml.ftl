<#ftl output_format="HTML">
<html>
<head>
  <title>検索結果</title>
  <style>
    span.matched {
      color: red;
      font-weight: bold;
    }

    .matchedCount .label {
      font-weight: bold;
    }

    table {
      border-collapse: collapse;
    }

    table, td, th {
      border: solid black thin;
    }

    th {
      background-color: #ffffcc;
    }

    tbody tr:nth-child(2n+1) {
      background-color: #ffffff;
    }

    tbody tr:nth-child(2n) {
      background-color: #e6e6e6;
    }

    tbody td:last-child {
      white-space: pre-wrap;
    }
  </style>
</head>
<body>
  <div class="matchedCount">
    <span class="label">該当件数：</span>
    <span class="value">${results?size} 件</span>
  </div>
  <table>
    <thead>
      <tr>
        <th>パス</th>
        <th>ファイル名</th>
        <th>シート</th>
        <th>セル</th>
        <th>テキスト</th>
      </tr>
    </thead>
    <tbody>
    <#list results as cellInfo>
      <tr>
        <#assign linkUrl=util.getLinkUrl(cellInfo) />
        <td><a href='${linkUrl}'>${cellInfo.file.absolutePath}</a></td>
        <td><a href='${linkUrl}'>${cellInfo.file.name}</a></td>
        <td><a href='${linkUrl}'>${cellInfo.sheetName}</a></td>
        <td><a href='${linkUrl}'>${cellInfo.cellAddress}</a></td>
        <td>${util.formatMatchedText(cellInfo.matchedText)?no_esc}</td>
      </tr>
    </#list>
    </tbody>
  </table>
</body>
</html>
