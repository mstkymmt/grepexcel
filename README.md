# Excel Grep ツール

POI を使って Excel ファイルの grep・一括置換 をするツールです。

## 実行環境

* OS： Windows / Mac / Linux
* Java 8 または Java 11 以降

Java 8 の場合は、別途 JavaFX のランタイムが必要です。

* Oracle JDK 8 には、JavaFX が同梱されています。
* OpenJDK 8 の場合は OpenJFX 8 をインストールしてください。


## 実行方法

以下のように実行します。

```
java -jar grepexcel-0.1.0.jar
```

## 使用方法

### 検索

1. 画面で以下を入力します。

* ディレクトリ  
  検索するディレクトリです。ここで指定したディレクトリとそのサブディレクトリのエクセルファイルを検索対象とします。
* 検索  
  検索する文字列を正規表現で指定します。

2. 入力後、「検索」ボタンをクリックすると検索を実行して、結果を画面下部に一覧表示します。

一覧の行をダブルクリックすると当該ファイルを開きます。

### 一括置換

1. 画面で以下を入力します。

* ディレクトリ  
  検索するディレクトリです。ここで指定したディレクトリとそのサブディレクトリのエクセルファイルを検索対象とします。
* 検索  
  検索する文字列を正規表現で指定します。
* 置換  
  置き換える文字列を指定します。

2. 入力後、「一括置換」ボタンをクリックすると実行を開始します。  
   実行後、画面下部の一覧には該当した部分（置換前）が表示されます。

置換後のセルの書式は、置換結果の文字列によって以下のように変更されます。（基本的に Excel の置換と同様です）

|                  置換後の文字列                   | セル形式 |
| ------------------------------------------------- | -------- |
| 日時形式（yyyy/m/d, hh:mm:ss, yyyy/m/d hh:mm:ss） | 日付     |
| '=' で始まる                                      | 数式     |
| 数値形式（符号 + 数字と . のみ）                  | 数値     |
| その他                                            | 文字列   |

* 置換時は念のため、バックアップをしてください。
* 先頭が ' の場合は常に文字列として扱います。

### 検索結果のコピー

画面下部の一覧を HTML 形式でクリップボードにコピーできます。

検索結果が表示されている状態で、画面上部のメニューから「Edit」-「Copy Results as HTML」を選択します。

コピーしたものは Excel にペーストできます。
Excelにペーストすると、結果（パス列～セル列）のクリックで該当セルを表示できます。

## 制限

現状では、以下の制限があります。

* 検索対象はセルのみ（シェイプのテキスト、セルのコメントなどは対象外）
* 置換した場合、（一部の文字だけ赤字にするなどの）セル内で文字単位に設定した書式は失われます。

## ビルド

ビルドについては Java SDK 11 以降が必要です。（Java 8 ではビルドできません。）

### Gradle によるビルド＋実行

以下のコマンドを実行すると、ビルド実行後にアプリが起動します。

```
./gradlew run
```

### 実行用Jar ファイルの作成

実行用 jar ファイルは以下で作成できます。

```
./gradlew bootJar
```

実行すると以下のディレクトリに jar ファイルが作成されます。

```
build/libs/grepexcel-0.1.0.jar
```

## Eclipse での使用

Eclipse で編集する場合は、以下の2通りの方法があります。

* Eclipse の Gradle プラグイン（Buildship）
* Gradle で Eclipse のプロジェクトファイルを作ってインポート

### Eclipse の Gradle プラグインによるインポート

Gradle プロジェクトとして import してください。

1. メニューから File - Import... を選択
2. Gradle - Existing Gradle Project を選択して、Next をクリック
3. Next をクリック
4. Project Root Directory でプロジェクトのディレクトリ（build.gradle のある場所）を選択して、 Finish をクリック

### Gradle で Eclipse のプロジェクトファイルを作ってインポート

以下を実行します。

```
./gradlew eclipse
```

これで Eclipse のプロジェクトファイルが作成されるので、Eclipse で既存のプロジェクトとしてインポートします。

### プロジェクトの設定について

このツールでは GUI に JavaFX を使用していますが、Eclipse では JavaFX のクラスがアクセス禁止状態となっていて、デフォルトの設定ではビルドエラーとなります。

このため、Eclipse のプロジェクトの設定で以下を変更しています。（Gradle で変更しているので、import すれば変更された状態になります。）

* Java Compiler - Error/Warnings の Deprecated and restricted API
  * 「Forbidden reference」を Error → Ignore に変更

### 実行

実行する場合は以下のクラスを Java Application として実行します。

```
com.sample.grepexcel.GrepExcelApplication
```
